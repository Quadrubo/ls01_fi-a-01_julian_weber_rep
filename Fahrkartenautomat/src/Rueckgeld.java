
public class Rueckgeld {
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       r�ckgabebetrag = Math.round(100 * r�ckgabebetrag) / 100.00;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO wird in folgenden M�nzen ausgezahlt:\n", r�ckgabebetrag);

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
		          r�ckgabebetrag = Math.round(100 * r�ckgabebetrag) / 100.00;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
		          r�ckgabebetrag = Math.round(100 * r�ckgabebetrag) / 100.00;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
		          r�ckgabebetrag = Math.round(100 * r�ckgabebetrag) / 100.00;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	 	          r�ckgabebetrag = Math.round(100 * r�ckgabebetrag) / 100.00;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
		          r�ckgabebetrag = Math.round(100 * r�ckgabebetrag) / 100.00;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	 	          r�ckgabebetrag = Math.round(100 * r�ckgabebetrag) / 100.00;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	}
}
