import java.util.Scanner;

public class Bestellung {
	public static double fahrkartenbestellungErfassen() {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Anzahl der Tickets (EURO): ");
	    double anzahl = scanner.nextDouble();
	    while(anzahl < 1 || anzahl > 10) {
	    	System.out.println("Ey man darf nur zwischen 1 und 10 Tickets kaufen.");
	    	System.out.print("Anzahl der Tickets (EURO): ");
		    anzahl = scanner.nextDouble();
	    }
	    System.out.print("Einzelpreis des Tickets (EURO): ");
	    double einzelpreis = scanner.nextDouble();
	    return(einzelpreis * anzahl);
	       
	}
}
