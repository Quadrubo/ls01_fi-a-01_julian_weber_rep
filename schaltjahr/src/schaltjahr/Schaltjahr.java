package schaltjahr;
import java.util.Scanner;


public class Schaltjahr {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ein Jahr ein: ");
		int year = scanner.nextInt();
		
		scanner.close();
		
		if(year <= 1582) {
			if((year % 4) == 0) {
				System.out.println("Das Jahr " + year + " ist ein Schaltjahr.");
			}else {
				System.out.println("Das Jahr " + year + " ist kein Schaltjahr.");
			}
		}else if((year % 4) == 0) {
			if((year % 100) != 0){
				System.out.println("Das Jahr " + year + " ist ein Schaltjahr.");
			}else if((year % 400) == 0) {
				System.out.println("Das Jahr " + year + " ist ein Schaltjahr.");
			}else {
				System.out.println("Das Jahr " + year + " ist kein Schaltjahr.");
			}
		}else {
			System.out.println("Das Jahr " + year + " ist kein Schaltjahr.");
		}
	}
	
}
